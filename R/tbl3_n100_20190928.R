#' Simulation output (tbl3_n100_20190928 array) 
#'
#' data object obtained from `simsalapar`.
#' Refer **doOne3.R**
#'
#' @docType data
#'
#' @usage data(tbl3_n100_20190928)
#'
#' @format array
#'
#' @keywords datasets
#'
"tbl3_n100_20190928"