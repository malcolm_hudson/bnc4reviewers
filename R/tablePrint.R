#' LaTeX tables of results (Table 1)
#' 
#' @description 
#'     This function creates a LaTeX Table from an array of simulation results (e.g. ipar x n x ... ).
#'     The array may be obtained using the **bnc** package routines for BVN($\mu$, $\Sigma$).
#'     The table is formed using R package **simsalapar**.
#'     See also functions `table1()` and `simul1()`.
#' @param val named array with cell entries an outcome parameter 
#'   with dimnames specifying simulation design factors and 
#'   the  variable `isim` (indexing replication). 
#'   The Table will present summary values (default *median*) by row-col cell.
#' @param row.vars,col.vars variables selected for the table's rows and columns
#' @param digits table format, number of digits 
#' @param fun summary function of replicate values
#' @param caption.nam,sub character strings, table title, subheader
#' @param label LaTeX table label 
#' 
#' @return LaTeX table code (requires package **booktabs**)
#' @import simsalapar
#' @export
#' 
tablePrint <- function(
  val,
  row.vars=c("cs","rho"),
  col.vars=c("beta2"), # defaults appropriate for Table 1
  caption.nam="Table of input array",
  sub="constructed with the \\code{ftable} method \\code{toLatex.ftable}.",
  digits=2,
  fun=median, 
  label = "tab:ft"
  ){
  non.sim.margins <-setdiff(names(dimnames(val)), "n.sim") # "isim"
  val.med <- apply(val, non.sim.margins, fun, na.rm=T)
  fval <- formatC(val.med, digits=digits, format="f")
  ft <- ftable(fval, row.vars=row.vars, col.vars=col.vars)
  
  varList <- dimnames2varlist(dimnames(val.med))
  
  tabL <- utils::toLatex(
    ft, 
    vList = varList,
    fontsize = "scriptsize",
    caption = caption.nam, 
    sub=sub,
    label = label
  )
  return(tabL)
}

#' LaTeX tables of results (Table 1)
#' 
#' @description 
#'     This function provides a summary of an array of simulation results (e.g. ipar x n x ... ).
#'     The array may be obtained using the **bnc** package routines for BVN($\mu$, $\Sigma$).
#'     The array is converted to a data frame using R function `array2df` (package **simsalapar**)
#'     and a summary output.
#'     See also functions `table1()` and `simul1()`.
#' @param tbl named array with cell entries an outcome parameter 
#'   with dimnames specifying simulation design factors and 
#'   the  variable `isim` (indexing replication). 
#' @param ipar parameter of interest
#' 
#' @return a summary of the array (as data frame).
#' @import simsalapar
#' @export
#'
review <- function(ipar,tbl) { # tbl an array; table, =ifelse(table==1,bnc4Reviewers::tbl1_n1000,bnc4Reviewers::tbl3_n1000)
  tbl.df <- array2df(tbl) # [1] "n"     "beta2" "cs"    "rho"   "isim"  "ipar"  "value"
  nc <- dim(tbl.df)[2]
  par_nam <- dimnames(tbl)[[nc-1]][ipar] 
  apply(tbl[,,,,,ipar], par_nam, summary) 
  tbl.df <- array2df(tbl[,,,,,par_nam])
  summary(tbl.df)
}

#' tabL1 LaTeX for Table 1
#' 
#' @description 
#'     This function creates the LaTeX for the paper's Table 1 
#'     from an array of simulation results (e.g. n.par x num.n x ... n.sim).
#'     The array was obtained using the **bnc** package routines for BVN($\mu$, $\Sigma$).
#'     The table is formed using R package **simsalapar**.
#'     See also functions `table1()` and `simul1()`.
#' @param tbl1 output array of function `table1()`, calling package **bnc** 
#' @param caption.nam,sub character strings, table title, subheader
#' 
#' @return LaTeX table code (requires package **booktabs**)
#' @import simsalapar
#' @export
#'
tabL1 <- function(tbl1,  #=bnc4Reviewers::tbl1_n1000 dimnames: n,beta2,cs,rho,isim,ipar
  # $ipar
  # [1] "mu1"  "mu2"  "sd1"  "sd2"  "rho"  "itr"  "L2"   
  # [8] "N0"   "N1"   "N2"  "L0"   
  # [12] "mu10" "mu20" "s10"  "s20"  "rho0" "itr0" "L1"
  caption.nam=list("rho estimate","iteration","sd-event-of-interest"),
  sub="n=1000") {
  tabL1a <- tablePrint(tbl1[,,,,,5],row.vars=c("cs","rho"),col.vars=c("beta2"),caption.nam=caption.nam[[1]],sub=sub)
  tabL1b <- tablePrint(tbl1[,,,,,6],row.vars=c("cs","rho"),col.vars=c("beta2"),digits=0, fun=max, caption.nam=caption.nam[[2]],sub=sub)
  tabL1c <- tablePrint(tbl1[,,,,,3],row.vars=c("cs","rho"),col.vars=c("beta2"),caption.nam=caption.nam[[3]],sub=sub)
  tabL1.ltx <- list(tabL1a,tabL1b,tabL1c)
  return(tabL1.ltx)
}
#' tabL3 LaTeX for Table 3
#' 
#' @description 
#'     This function creates the LaTeX for the paper's Table 3 
#'     from an array of simulation results (e.g. ipar x n x ... ).
#'     The array was obtained using the **bnc** package routines for BVN($\mu$, $\Sigma$).
#'     The table is formed using R package **simsalapar**.
#'     See also functions `table3()` and `simul3()`.
#' @param tbl3 output array of function `table3()`, calling package **bnc** 
#' @param caption.nam,sub character strings, table title, subheader
#' 
#' @return LaTeX table code (requires package **booktabs**)
#' @import simsalapar
#' @export
#'
tabL3 <- function(tbl3=bnc4Reviewers::tbl3_n1000, 
  caption.nam=list("b12 estimate","b21 TreatDiff estimate","rho estimate"),
  sub="n=1000") {
  tabL3a <- tablePrint(tbl3[1,,,,,,3],row.vars=3:4,col.vars=c(2,1),caption.nam=caption.nam[[1]],sub=sub)
  tabL3b <- tablePrint(tbl3[1,,,,,,2],row.vars=3:4,col.vars=c(2,1),caption.nam=caption.nam[[2]],sub=sub)
  tabL3c <- tablePrint(tbl3[1,,,,,,7],row.vars=3:4,col.vars=c(2,1),caption.nam=caption.nam[[3]],sub=sub)
  tabL3.ltx <- list(tabL3a,tabL3b,tabL3c)
  return(tabL3.ltx)
}
