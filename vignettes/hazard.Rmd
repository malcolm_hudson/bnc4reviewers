---
title: "BNC hazard functions"
date: "9 March 2019"
author: "Malcolm Hudson"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{BNC hazard functions}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
  %\VignetteDepends{bnc,survival}
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
knitr::opts_chunk$set(eval = TRUE)
knitr::opts_chunk$set(fig.width=7,fig.height=5)
knitr::opts_chunk$set(fig.path = here::here('bncPaperResults/'))
knitr::opts_chunk$set(dev='cairo_pdf')
```

```{r loadCode, echo=FALSE, message=FALSE, warning=FALSE}
# library(BivarP)
library(bnc4Reviewers)
# library(bnc)
# library(here)
# library(Cairo)
```

```{r }
# bnc:::PsiFn()
PsiFn <- function(z) {
  return(dnorm(z) / pnorm(z,lower = F))
}
lambFGv <- Vectorize(bnc::lambFG,"y")
```


```{r initial}
t=seq(0.1,exp(1),0.1)
beta=c(0,0.5,1)
hazard_ratio = matrix(0,ncol= length(beta),nrow= length(t))

```


# Hazard ratio

We work towards calculating the hazard ratio comparing Treatment with Control in two cases:

- cause-specific hazards;
- sub-distribution hazards of Fine and Gray.

The calculation is described in the paper's Methods, and involves functions (notably `PsiFn`, `Svf` and `cif()`) contained in the package `bnc4Reviewers`, in file `utils.R`

## Univariate hazard function

First, calculate hazard functions with respect to Y for *independent* competing risks.
In this case, the result is that for a *univariate* survival time with log-Normal distribution.

```{r hazard_Y}
y=seq(-2,2,0.1)
Delta=c(0,0.5,1)
PsiFn <- function(z) {
  return(dnorm(z) / pnorm(z,lower = F))
}
plot(y,PsiFn(y),xlab="Time (log)",ylab="Hazard",type="l",ylim=c(0.01,3),log="y")
lines(y,PsiFn(y-0.5), col=2)
lines(y,PsiFn(y-1), col=3)
legend("bottomright", horiz = TRUE,
       legend=c(expression(Delta == 0),
                expression(Delta == 0.5),
                expression(Delta == 1)
                ), 
       col = 1:3,
       lty=1
       )
```

The corresponding hazard function for $T=\exp Y$ is obtained by the relationship
$$
h_T (t) = h_Y (y) \frac{dy}{dt} = h_Y(\log t)/t
$$
where $y=\log t$. This hazard is shown below, on the original time scale (for t).

```{r hazard_expY}
y=seq(-2,2,0.1)
t=exp(y)
Delta=c(0,0.5,1)
plot(t,PsiFn(y) / t,
     xlab="Time (log scale)",ylab="Hazard",type="l",ylim=c(0.01,3),
     log="xy")
lines(t,PsiFn(y-0.5) / t, col=2)
lines(t,PsiFn(y-1) / t, col=3)
legend("bottomright", horiz = TRUE,
       legend=c(expression(Delta == 0),
                expression(Delta == 0.5),
                expression(Delta == 1)
                ), 
       col = 1:3,
       lty=1
       )
```

## Bivariate cause-specific hazards

For $\Delta=0.5$ and late occurring competing event, HR depends on the correlation $\rho$ between times to events 1 and 2: we consider cases of late occurring competing risks ($\mu_2=2$) -- where there is little censoring of the event of interest by the competing risk -- and early occurring competing risks ($\mu_2=0$) where about half of the events of interest are censored by the competing risk.

### Late occurring competing event

We plot (true) CS hazard as a function of time for control and treated groups.

```{r Late}
# Treatment effect \hat{Delta}=0.5
# rho 0.5
# fitted CS hazard for control group
csh1.2.05.0 <- bnc::bnc_lambda(y,mu1=0,mu2=2,sigma1=1,sigma2=1, rho=0.5) # event 1, local recurrence
csh1.2.05.1 <- bnc::bnc_lambda(y,mu1=0.5,mu2=2,
   sigma1=1,sigma2=1, rho=0.5) # ev. 1, local 2, non-local
# rho 0
# fitted CS hazard for control group
csh1.2.00.0 <- bnc::bnc_lambda(y,mu1=0,mu2=2,sigma1=1,sigma2=1, rho=0) # event 1, local recurrence
csh1.2.00.1 <- bnc::bnc_lambda(y,mu1=0.5,mu2=2, sigma1=1,sigma2=1, rho=0) # ev. 1, local 2, non-local
# fitted CS hazard for control group
csh1.2.n5.0 <- bnc::bnc_lambda(y,mu1=0,mu2=0,sigma1=1,sigma2=1, rho=-0.5) # event 1, local recurrence
csh1.2.n5.1 <- bnc::bnc_lambda(y,mu1=0.5,mu2=0, sigma1=1,sigma2=1, rho=-0.5) # ev. 1, local 2, non-local
plot(t, csh1.2.05.0/ t,
     xlab="Time (log scale)",ylab="Hazard",type="l",ylim=c(0.01,2),lty=2,
     log="xy")
lines(t,csh1.2.05.1 / t, col=2,lty=2)
lines(t,csh1.2.00.0 / t, col=1)
lines(t,csh1.2.00.1 / t, col=2) # near overlay of rho=0.5
lines(t,csh1.2.n5.0 / t, col=5,lty=2)
lines(t,csh1.2.n5.1 / t, col=6,lty=2) # overlay 
legend("bottom", 
       legend=paste(rep(c("Control","Treat"),times=3),rep(c("rho=0.5","rho=0","rho=-0.5"),each=2),sep=", "),
       col = c(1,2,1,2,5,6), 
       lty=c(2,2,1,1,2,2)
       )
```

Note the overlay of hazards with $\rho=0.5$ over $\rho=0$ when late occurring events provide almost no censoring of event 1. 

### Early occurring competing event

With early occurring competing events,

```{r }
# mu2=0, rho 0.5
# fitted CS hazard for control group
csh1.0.05.0 <- bnc::bnc_lambda(y,mu1=0,mu2=0,sigma1=1,sigma2=1, rho=0.5) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
csh1.0.05.1 <- bnc::bnc_lambda(y,mu1=0.5,mu2=0, sigma1=1,sigma2=1, rho=0.5) # ev. 1, local 2, non-local
# fitted CS hazard for control group
csh1.0.00.0 <- bnc::bnc_lambda(y,mu1=0,mu2=0,sigma1=1,sigma2=1, rho=0) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
csh1.0.00.1 <- bnc::bnc_lambda(y,mu1=0.5,mu2=0, sigma1=1,sigma2=1, rho=0) # ev. 1, local 2, non-local
# fitted CS hazard for control group
csh1.0.n5.0 <- bnc::bnc_lambda(y,mu1=0,mu2=0,sigma1=1,sigma2=1, rho=-0.5) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
csh1.0.n5.1 <- bnc::bnc_lambda(y,mu1=0.5,mu2=0, sigma1=1,sigma2=1, rho=-0.5) # ev. 1, local 2, non-local

plot(t, csh1.0.05.0/ t,
     xlab="Time (log scale)",ylab="Hazard",type="l",ylim=c(0.01,2),lty=2,
     log="xy")
# lines(t,csh1.0.05.0 / t, col=4)
lines(t,csh1.0.05.1 / t, col=2,lty=2)
lines(t,csh1.0.00.0 / t, col=1)
lines(t,csh1.0.00.1 / t, col=2) # overlay 
lines(t,csh1.0.n5.0 / t, col=5,lty=2)
lines(t,csh1.0.n5.1 / t, col=6,lty=2) # overlay 
legend("bottom", 
       legend=paste(rep(c("Control","Treat"),times=3),rep(c("rho=0.5","rho=0","rho=-0.5"),each=2),sep=", "),
       col = c(1,2,1,2,5,6), 
       lty=c(2,2,1,1,2,2)
       )
```

Hazards for $\rho=0$ remain unchanged with early occurring competing events.
For positive and negative correlations, hazards diverge from those for $\rho=0$, increasingly so as time increases. 

Note that the HR for $\rho=0$ is readily calculable as a function of $\Delta$.
The dashed line is the HR with $\rho=0$ with early occurring competing events. This line overlays the computed result of `PsiFn(y-Delta)/PsiFn(y)`.

```{r HR_yscale}
plot(y,PsiFn(y-1)/PsiFn(y),xlab="Time (log)",ylab="Hazard ratio",type="l",ylim=c(0.05,1.25), log="y",col=3)
lines(y,PsiFn(y-0.5)/PsiFn(y),col=2)
lines(y, csh1.0.00.1/csh1.0.00.0, col=1, lw=2, lty=2)
abline(h=1.00)
legend("bottomright", 
       horiz = TRUE, 
       legend=c(expression(Delta == 0),
                expression(Delta == 0.5),
                expression(Delta == 1)), col = c(1, 2, 3), 
       lty=1 #,box.lty=0
       )
```

The HR is independent of whether hazard is calculated on the original time scale or the log-time scale.

## Hazard ratios with early occurring competing events

HRs are time dependent, with best risk reduction early,
and $\rho=0.5$ achieving slightly better HRs than the corresponding result for independent censoring $\rho=0$ or $\rho=-0.5$.
The hazard ratio reduction (HRR=1-HR) decreases monotonically.
We save the result as **bncPaperResults/HazardRatio.pdf**.

```{r }
# pdf(here("Figures/","HazardRatio.pdf")) 
plot(0.1,0.1,xlim=c(exp(-2),exp(2)),ylim=c(0.25,1),xlab="Time (years)",ylab="HR (CS)",log="xy",type="n")
lines(t, csh1.0.00.1/csh1.0.00.0,lw=2, col=1) # csh1.2.05.1/csh1.2.05.0
#lines(t, csh1.2.00.1/csh1.2.00.0,lw=2, col=1,lty=2)
lines(t, csh1.0.05.1/csh1.0.05.0,lw=2, col=2) # differs!
#lines(t, csh1.0.00.1/csh1.0.00.0,lw=2, col=2,lty=2)
lines(t, csh1.0.n5.1/csh1.0.n5.0,lw=2, col=2,lty=2)
legend("bottomright", 
       legend=c("rho=0.5","rho=0","rho=-0.5"), 
       col = c(2,1,2), 
       lty=c(1,1,2)
       )
# sudo apt-get install libcairo2-dev
# install.package(Cairo)
# cairo_pdf(filename = here("bncPaperResults/","HazardRatio.pdf"))
# dev.off()
```


## Fine and Gray sub-distribution hazards (SDHs) 

```{r hazFG} 
# rho 0.5
# fitted CS hazard for control group
FGh1.2.05.0 <- lambFGv(y,mu1=0,mu2=2,sigma1=1,sigma2=1, rho=0.5) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
FGh1.2.05.1 <- lambFGv(y,mu1=0.5,mu2=2,
   sigma1=1,sigma2=1, rho=0.5) # ev. 1, local 2, non-local
# rho 0
# fitted FG hazard for control group
FGh1.2.00.0 <- lambFGv(y,mu1=0,mu2=2,sigma1=1,sigma2=1, rho=0) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
FGh1.2.00.1 <- lambFGv(y,mu1=0.5,mu2=2, sigma1=1,sigma2=1, rho=0) # ev. 1, local 2, non-local
# rho -0.5
# fitted CS hazard for control group
FGh1.2.n5.0 <- lambFGv(y,mu1=0,mu2=2,sigma1=1,sigma2=1, rho=-0.5) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
FGh1.2.n5.1 <- lambFGv(y,mu1=0.5,mu2=2,
   sigma1=1,sigma2=1, rho=-0.5) # ev. 1, local 2, non-local

# mu2=0, rho 0.5
# fitted FG hazard for control group
FGh1.0.05.0 <- lambFGv(y,mu1=0,mu2=0,sigma1=1,sigma2=1, rho=0.5) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
FGh1.0.05.1 <- lambFGv(y,mu1=0.5,mu2=0, sigma1=1,sigma2=1, rho=0.5) # ev. 1, local 2, non-local
# fitted FG hazard for control group
FGh1.0.00.0 <- lambFGv(y,mu1=0,mu2=0,sigma1=1,sigma2=1, rho=0) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
FGh1.0.00.1 <- lambFGv(y,mu1=0.5,mu2=0, sigma1=1,sigma2=1, rho=0) # ev. 1, local 2, non-local
# fitted FG hazard for control group
FGh1.0.n5.0 <- lambFGv(y,mu1=0,mu2=0,sigma1=1,sigma2=1, rho=-0.5) # event 1, local recurrence
# fit for treated: \hat{Delta}=0.5
FGh1.0.n5.1 <- lambFGv(y,mu1=0.5,mu2=0, sigma1=1,sigma2=1, rho=-0.5) # ev. 1, local 2, non-local

plot(t, FGh1.2.05.0/ t,
     xlab="Time (log scale)",ylab="Hazard",type="l",ylim=c(0.01,2),lty=2,
     log="xy")
lines(t,FGh1.2.05.1 / t, col=2,lty=2)
lines(t,FGh1.2.00.0 / t, col=1)
lines(t,FGh1.2.00.1 / t, col=2) # near overlay of rho=0.5
lines(t,FGh1.2.n5.0 / t, col=5,lty=2)
lines(t,FGh1.2.n5.1 / t, col=6,lty=2) # overlay 
legend("bottomleft", 
       legend=paste(rep(c("Control","Treat"),times=3),rep(c("rho=0.5","rho=0","rho=-0.5"),each=2),sep=", "),
       col = c(1,2,1,2,5,6), 
       lty=c(2,2,1,1,2,2)
       )
plot(t, FGh1.0.05.0/ t,
     xlab="Time (log scale)",ylab="Hazard",type="l",ylim=c(0.01,2),lty=2,
     log="xy")
# lines(t,FGh1.0.05.0 / t, col=4)
lines(t,FGh1.0.05.1 / t, col=2,lty=2)
lines(t,FGh1.0.00.0 / t, col=1)
lines(t,FGh1.0.00.1 / t, col=2) # overlay 
lines(t,FGh1.0.n5.0 / t, col=5,lty=2)
lines(t,FGh1.0.n5.1 / t, col=6,lty=2) # overlay 
legend("bottomleft", 
       legend=paste(rep(c("Control","Treat"),times=3),rep(c("rho=0.5","rho=0","rho=-0.5"),each=2),sep=", "),
       col = c(1,2,1,2,5,6), 
       lty=c(2,2,1,1,2,2)
       )
```

## Sub-distribution hazard ratios

The SDHR appears to increase consistently from early near 0.25 (treatment benefit early) to HR exceeding 1 (and as much as 5).
This late occurring treatment harm is not apparent with correlation $\rho=0.5$ as the SDHR remains below 1.


```{r HR_FG}
plot(0.1,0.1,xlim=c(exp(-2),exp(2)),ylim=c(0.1,5),xlab="Time(yrs)",ylab="HR (FG)",log="xy",type="n")
lines(t, FGh1.0.00.1/FGh1.0.00.0,lw=2, col=1) # FGh1.2.05.1/FGh1.2.05.0
#lines(t, FGh1.2.00.1/FGh1.2.00.0,lw=2, col=1,lty=2)
lines(t, FGh1.0.05.1/FGh1.0.05.0,lw=2, col=2) # differs!
#lines(t, FGh1.0.00.1/FGh1.0.00.0,lw=2, col=2,lty=2)
lines(t, FGh1.0.n5.1/FGh1.0.n5.0,lw=2, col=2,lty=2)
legend("bottomright", 
       legend=c("rho=0.5","rho=0","rho=-0.5"), 
       col = c(2,1,2), 
       lty=c(1,1,2)
       )
```

<!-- For further explorations of BNC model hazards, -->
<!-- see the vignette `HR assumptions` (HRassumptions.Rmd). -->